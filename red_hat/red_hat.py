import pandas as pd
from sklearn.ensemble import RandomForestClassifier as RandomForest
from sklearn.metrics import roc_curve, auc
from sklearn.svm import SVC

def load_people(filename):
	people = pd.read_csv(filename)
	people = people.drop(['date'], axis=1)
	people['people_id'] = people['people_id'].apply(lambda x: x.split('_')[1])
	people['people_id'] = pd.to_numeric(people['people_id']).astype(int)
	people = people.fillna('type 0')

	bools = people.columns[11:]
	strings = people.columns[1:11]

	for col in bools:
		people[col] = pd.to_numeric(people[col]).astype(int)

	for col in strings:
		people[col] = people[col].apply(lambda x: x.split(' ')[1])
		people[col] = pd.to_numeric(people[col]).astype(int)

	return people

def load_act(filename):
	act = pd.read_csv(filename, parse_dates=['date'], date_parser=pd.to_datetime)
	act = act.drop(['activity_id'], axis=1)
	act = act.fillna('type 0')

	act['people_id'] = act['people_id'].apply(lambda x: x.split('_')[1])
	act['people_id'] = pd.to_numeric(act['people_id']).astype(int)

	for col in act.columns[2:13]:
		act[col] = act[col].apply(lambda x: x.split(' ')[1])
		act[col] = pd.to_numeric(act[col]).astype(int)

	return act

def train_random_forest(data, num_trees):
	random_forest = RandomForest(n_jobs=3, n_estimators=num_trees)
	random_forest.fit(merged_train, outcome_train)

	return random_forest


people = load_people('people.csv')
act_train = load_act('act_train.csv')
act_test = load_act('act_test.csv')
N_test = float(len(act_test))

merged_train = people.merge(act_train, on='people_id')
outcome_train = merged_train['outcome']
merged_train = merged_train.drop(['date', 'outcome'], axis=1)

merged_test = people.merge(act_train, on='people_id')
outcome_test = merged_test['outcome']
merged_test = merged_test.drop(['date', 'outcome'], axis=1)

# random forest
random_forests = [train_random_forest(merged_test, num_trees) for num_trees in range(10,11)]
random_forest = random_forests[0]

preds = random_forest.predict_proba(merged_test)[:,1]
fpr, tpr, thresh = roc_curve(outcome_test, preds)
plot(thresh, tpr, thresh, fpr)
plot(fpr, tpr)

"""
#support vector machine
svc = SVC()
svc.fit(merged_train, outcome_train)
outcome_predicted = pd.Series(svc.predict(merged_test))
error_svc = sum(abs(outcome_predicted - outcome_test)) / N_test

outcome_predicted = pd.Series(random_forest.predict(merged_test))
error_rf = sum(abs(outcome_predicted - outcome_test)) / N_test
"""